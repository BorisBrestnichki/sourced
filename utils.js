function removeButOne(parent, one) {
  parent.childNodes.forEach(child => {
    if (child !== one && child.nodeType == 1) child.classList.add("removed");
  });
  one.classList.remove("removed");
}

module.exports = {
  removeButOne: removeButOne
};
