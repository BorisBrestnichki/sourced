import * as firebase from "firebase/app";
import "firebase/database"
 
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDq219deRms-5wMkpdyU4NUKM6BVtFr4Nc",
    authDomain: "sourced-21049.firebaseapp.com",
    databaseURL: "https://sourced-21049.firebaseio.com",
    projectId: "sourced-21049",
    storageBucket: "",
    messagingSenderId: "15957670456",
    appId: "1:15957670456:web:02e7b51fd3cba0bd5384cd"
};

firebase.initializeApp(firebaseConfig);

module.exports = firebase.database()