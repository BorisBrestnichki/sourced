import * as api from "./api";
import setupOrigins from "./producer_origins";

(async () => {
  var loginWrapper = document.querySelector(".login_warpper");

  let user = getCookie("loged");

  if (user) {
    let userObject = JSON.parse(user);
    await api.setupUser(userObject.name);
    removeLogin(userObject);
    return;
  }

  loginWrapper.classList.remove("removed");

  let btn = document.querySelector("#login_button");
  btn.addEventListener("click", async event => {
    let val = document.querySelector("#login").value;

    if (!val || val == "") {
      error();
    }

    let result = await api.setupUser(val).catch(err => error());

    console.log(result);

    if (result) {
      removeLogin(result);
    }

    function error() {
      btn.classList.add("error");
      setTimeout(() => {
        btn.classList.remove("error");
      }, 1000);
    }
  });

  function removeLogin(result) {
    loginWrapper.classList.add("removed");
    document.querySelector(".content_wrapper").classList.remove("removed");
    setCookie("loged", JSON.stringify(result), 1);
    setupOrigins();
  }
})();

function setCookie(name, value, days) {
  var d = new Date();
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
  document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

function getCookie(name) {
  var v = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
  return v ? v[2] : null;
}
