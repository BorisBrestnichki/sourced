import utils from "./utils";
import "./api";

var inputWrapper = document.querySelector(".content_wrapper .input_wrapper");
var treatmentInput = inputWrapper.querySelector(".treatment_input_wrapper");
var dateInput = document.querySelector("input[name=treatment_date]");
let addTreatmentButton = inputWrapper.querySelector(".add_treatment_button");

var pesticidesSelect = document.querySelector("select[name=treatment");

api.getPesticides(pesticides => {
  Object.keys(pesticides).forEach(key => {
    pesticidesSelect.options[pesticidesSelect.options.length] = new Option(
      pesticides[key].name,
      key
    );
  });
});

addTreatmentButton.addEventListener("click", async event => {
  if (dateInput.value == "") {
    addTreatmentButton.classList.add("error");
    setTimeout(() => {
      addTreatmentButton.classList.remove("error");
    }, 1000);
    return;
  }

  await api.addTreatment(
    dateInput.value,
    event.currentTarget.getAttribute("data-id"),
    pesticidesSelect.value
  );

  dateInput.value = "";
});

function setupTreatments() {
  document.querySelectorAll(".add_field_treatments").forEach(button => {
    button.addEventListener("click", event => {
      let id = event.currentTarget.getAttribute("data-id");

      addTreatmentButton.setAttribute("data-id", id);

      utils.removeButOne(inputWrapper, treatmentInput);
    });
  });
}

module.exports = setupTreatments;
