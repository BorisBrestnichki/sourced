import database from "./firebase";

let producer;

async function setupUser(name) {
  let producerRef = await database
    .ref("producers")
    .orderByChild("name")
    .equalTo(name)
    .limitToFirst(1)
    .once("value");

  let producerVal = producerRef.val();

  producer = {
    id: Object.keys(producerVal)[0],
    ...producerVal[Object.keys(producerVal)[0]]
  };

  return producer;
}

async function addProducer(name) {
  await database.ref("producers").push({ name: name });
}

async function addOrigin(data) {
  database.ref(`producers/${producer.id}/origins`).push({
    name: data.name,
    location: data.location
  });
}

async function hookOrigins(callback) {
  database.ref(`producers/${producer.id}/origins`).on("value", snapshot => {
    callback(snapshot.val());
  });
}

async function addProduct(obj) {
  let pulledOrigin = producer.origins[obj.originKey];
  pulledOrigin.id = obj.originKey;

  let pulledIngridients = Object.keys(producer.products || {}).reduce(
    (acc, key) => {
      if (obj.ingridientsKeys.includes(key)) {
        acc.push(producer.products[key]);
      }
      return acc;
    },
    []
  );

  let modifiedProducer = { name: producer.name, id: producer.id };

  await database.ref(`products`).push({
    date: obj.date,
    name: obj.name,
    description: obj.description,
    unverifiedIngridients: obj.unverifiedIngridients,
    image: obj.image,
    producer: modifiedProducer,
    origin: pulledOrigin,
    ingridients: pulledIngridients.length == 0 ? false : pulledIngridients
  });
}

/*
setupUser("Gong1").then(() => {
  addProduct({
    date: "2019-11-15T12:00:00",
    description: "best_mongos",
    name: "Gonzo1",
    image: "/image.png",
    ingridientsKeys: ["-LoiEAWhK4ClpnavvHul"],
    unverifiedIngridients: "cucumber, monkeys",
    originKey: "-LogSQSDhQ9tkEcTMp_k"
  });
});
*/

async function addProducerProduct(productId) {
  let productRef = await database.ref(`products/${productId}`).once("value");
  let product = productRef.val();
  product.id = productId;
  await database.ref(`producers/${producer.id}/products`).push(product);
}
/*
setupUser("Gong1").then(() => {
  addProducerProduct("-Logopoih1i6a44Itnnc");
});
*/

async function addTreatment(date, originId, pesticideId) {
  let pesticideRef = await database
    .ref(`pesticides/${pesticideId}`)
    .once("value");

  let pesticide = { id: pesticideId, ...pesticideRef.val() };

  database.ref(`producers/${producer.id}/origins/${originId}/treatments`).push({
    date: date,
    pesticide: pesticide
  });
}

async function hookTreatments(originId, callback) {
  let treatsRef = await database
    .ref(`producers/${producer.id}/origins/${originId}/treatments`)
    .on("value", snapshot => {
      callback(snapshot.val());
    });
}

async function addPesticide(name, toxicity) {
  await database.ref(`pesticides`).push({
    name: name,
    toxicity: toxicity
  });
}

async function getPesticides(callback) {
  database.ref(`pesticides`).once("value", snapshot => {
    callback(snapshot.val());
  });
}

async function getProduct(id) {
  var productRef = await database.ref(`products/${id}`).once("value");

  return productRef.val();
}

/*
getProduct('-Logopoih1i6a44Itnnc').then(product => console.log(product))
*/

module.exports = {
  addProducer: addProducer,
  addOrigin: addOrigin,
  setupUser: setupUser,
  hookOrigins: hookOrigins,
  addProduct: addProduct,
  addProducerProduct: addProducerProduct,
  addPesticide: addPesticide,
  getPesticides: getPesticides,
  addTreatment: addTreatment,
  hookTreatments: hookTreatments,
  getProduct: getProduct
};

if (window) window.api = module.exports;

(async () => {
  await setupUser("Alex Brestnichki");
  /*
  addOrigin({
    name: "Field 1017",
    location: "51.215054, -2.886460"
  });
  */

  //addProducer("John Dow");
  //addTreatment("2019-8-11", "-Loij0YWBG7XTTOdqldM", "-LogTMDkjK63NHdP_isR");
  /*
  addProduct({
    name: "Liutenitza sause",
    originKey: "-LoijJHgHKCZcgm3GS0G",
    date: "2019-09-01",
    image: "https://www.thompson-morgan.com/p/tomato-moneymaker/282TM",
    description: "Liutenitza sause made from fresh ingredients",
    unverifiedIngridients: "",
    ingridientsKeys: ["-Loio9c2DzSw7gpvEDpq", "-LoioFxV4kCwCX-GAU6J"]
  });
  /*
   */
  //addProducerProduct("-LoinvQ2sEXGeb1RWf2o");
})();
