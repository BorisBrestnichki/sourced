import "./api";
import setupTreatments from "./setupTreatments";
import utils from "./utils";

function setupOrigins() {
  var originTemplate = document.querySelector(".fields_template");
  var originParent = originTemplate.parentNode;
  api.hookOrigins(origins => {
    originParent.childNodes.forEach(element => {
      if (element !== originTemplate) originParent.removeChild(element);
    });

    Object.keys(origins).forEach(key => {
      let newOrigin = originTemplate.cloneNode(true);
      newOrigin.classList.remove("vissualyhidden");
      newOrigin.querySelector(".field_name").textContent = origins[key].name;
      newOrigin.querySelector(".field_location").textContent =
        origins[key].location;
      newOrigin.querySelector(
        ".filed_treatments"
      ).textContent = getTreatmentsString(origins[key]);
      newOrigin
        .querySelector(".add_field_treatments")
        .setAttribute("data-id", key);
      originParent.appendChild(newOrigin);
      setupTreatments();
    });
  });

  var inputWrapper = document.querySelector(".content_wrapper .input_wrapper");
  var fieldInput = inputWrapper.querySelector(".field_input_wrapper");

  document.querySelector(".add_field").addEventListener("click", event => {
    utils.removeButOne(inputWrapper, fieldInput);
  });

  let addFieldButton = fieldInput.querySelector("#add_field_button");

  addFieldButton.addEventListener("click", event => {
    let fieldNameInput = document.querySelector("input[name=field_name]");
    let fieldLocationInput = document.querySelector(
      "input[name=field_location]"
    );

    if (fieldNameInput.value == "" || fieldLocationInput.value == "") {
      addFieldButton.classList.add("error");
      setTimeout(() => {
        addFieldButton.classList.remove("error");
      }, 1000);
      return;
    }

    api
      .addOrigin({
        name: fieldNameInput.value,
        location: fieldLocationInput.value
      })
      .then(() => {
        fieldNameInput.value = "";
        fieldLocationInput.value = "";
      });
  });
}

function getTreatmentsString(origin) {
  if (!origin.treatments) return "";

  return Object.keys(origin.treatments).reduce((acc, treatmentKey) => {
    acc += `${origin.treatments[treatmentKey].pesticide.name}, `;
    return acc;
  }, "");
}

module.exports = setupOrigins;
